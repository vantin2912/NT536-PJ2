<aside class="app-sidebar">
    <div class="sidebar-img">
        <a class="navbar-brand" href="index-2.html"><img alt="..." class="navbar-brand-img main-logo"
                src="./assets/img/brand/logo.png" />
            <img alt="..." class="navbar-brand-img logo" src="./assets/img/brand/logo.png" /></a>
            
        <ul class="side-menu">
            <li class="slide is-expanded">
                <a class="side-menu__item" data-toggle="slide" href="#"><i
                        class="side-menu__icon fas fa-house-user"></i><span
                        class="side-menu__label">IotHome</span><i class="angle fa fa-angle-right"></i></a>
                <ul class="slide-menu">
                    <li>
                        <a href="#" class="slide-item" id= "LoadLiving"><i
                                class="side-menu__icon fas fa-tv"></i>Phòng Khách</a>
                    </li>
                    <li>
                        <a href="#" class="slide-item" id= "LoadKitchen"><i
                                class="side-menu__icon fas fa-mitten"></i>Phòng bếp</a>
                    </li>
                </ul>
            </li>
            <li class ="slide"> 
            <a href="./VidStream.php" class="side-menu__item">
            <i class="side-menu__icon fas fa-video "> </i> 
            <span
                        class="side-menu__label">Conference </span>						
            </a>
            </li> 
            <li class ="slide">
            <a href="#" class="side-menu__item" id="LogOut">
            <i class="side-menu__icon fas fa-sign-out-alt "> </i> 
            <span class="side-menu__label">Đăng Xuất </span>				
            </a>
            </li> 
        </ul>

    </div>
</aside>