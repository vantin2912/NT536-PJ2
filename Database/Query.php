<?php
	require_once("DBConfig.php");
   
	$conn = mysqli_connect($servername, $username, $password, $database);
	mysqli_set_charset($conn, 'UTF8');
	
	if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		};
	function CheckUser($Email, $Password)
	{
		global $conn;
		$query = "SELECT UserID From user Where Email = '$Email' AND Password = '$Password' ";
		$result = mysqli_query($conn,$query);
		if(empty($result)) return;
		$data = mysqli_fetch_assoc($result);
		return $data["UserID"];
	};
	function getRoomStatus(int $RoomID)
	{
		global $conn;
		$query = "SELECT RoomName, Fan, Led, DeviceUpdate From roomstatus Where ID =$RoomID";
		$result = mysqli_query($conn,$query);
		$data = mysqli_fetch_assoc($result);
		return $data;
	}
	function getHomeStatus($ID)
	{
		global $conn;
		$query = "SELECT * From homestatus Where ID = $ID ";
		$result = mysqli_query($conn, $query);
		$data = mysqli_fetch_assoc($result);
		return $data;
	}
	function updateRoomStatus(int $RoomID, string $FanStatus, string $LedStatus, $DeviceUpdate = 0)
	{
		global $conn;
		$query = "UPDATE roomstatus SET Fan = \"$FanStatus \", Led = \"$LedStatus\", DeviceUpdate = $DeviceUpdate Where ID = $RoomID";
		mysqli_query($conn,$query);
		return $conn -> error;
	}
	function updateHouseStatus($Humid,$Temp,$Gas)
	{
		global $conn;
		$query = "UPDATE homestatus 
		SET Humid = $Humid, Temp = $Temp, Gas = $Gas
		Where ID = 1";

		mysqli_query($conn, $query);
		return $conn -> error;
	}
	function updateRoomDevice($RoomID, $DeviceName, $Status, $DeviceUpdate = 0)
	{
		global $conn;
		$query = "UPDATE roomstatus
		SET $DeviceName = \"$Status\", DeviceUpdate = $DeviceUpdate
		Where ID = $RoomID";
		// echo $query;
		mysqli_query($conn,$query);
		return $conn -> error;
	}

	function updateDoorStatus($Status)
	{
		global $conn;
		$query = " INSERT INTO doorhistory(Status) values(\"$Status\")";
		mysqli_query($conn,$query);
		return $conn->error;
	}

	function getDoorHistory($Row)
	{
		global $conn;
		if($Row == 0)
		{
			$query = "SELECT * 
			From DoorHistory
			ORDER BY TIME DESC";
		} else{
			$query = "SELECT * 
			From DoorHistory
			ORDER BY TIME DESC
			LIMIT $Row";
		}
		
		$result  = mysqli_query($conn,$query);
		$data = mysqli_fetch_all($result, MYSQLI_ASSOC);
		// print_r($data);
		echo $conn -> error;
		return $data;
	}
	function updateMotion($Motion)
	{
		global $conn;
		$query = "UPDATE homestatus SET Motion = \"$Motion\" Where ID = 1";
		mysqli_query($conn, $query);
		return $conn -> error;
	}

	function getUserInfo($UserID)
	{
		global $conn;
		// $query = "SELECT user.UserID, user.Username, user.FullName 
		// 			From videoroom 
		// 			inner join user on user.UserID = videoroom.UserID
		// 			Where RoomID =\"$RoomID\" AND TrackID = \"$TrackID\" ";
		$query = "SELECT UserID, Username, FullName
					from user
					where UserID=\"$UserID\"";
		$result = mysqli_query($conn,$query);
		$data = mysqli_fetch_assoc($result);
		
		return $data;
	}

	function addUser($UserName, $Email, $FullName, $Password)
	{
		global $conn;
		$query = "INSERT INTO user (Username, Email, FullName, Password)
					VALUES (\"$UserName\", \"$Email\", \"$FullName\", \"$Password\");";
		$result = mysqli_query($conn,$query);
		
		return $result;
	}

	function joinRoom($RoomID, $ConversationID)
	{
		global $conn;
		$query = "INSERT INTO roominfo (RoomID, ConversationID)
					VALUES (\"$RoomID\", \"$ConversationID\"); ";
		// echo $query;
		$result = mysqli_query($conn, $query);

		return $result;
	}

	function getRoomInfo($RoomID)
	{
		global $conn;
		$query = "SELECT * from roominfo where RoomID = \"$RoomID\" ";
		// echo $query;
		$result = mysqli_query($conn, $query);
		$data = mysqli_fetch_assoc($result);
		return $data;
	}
?>