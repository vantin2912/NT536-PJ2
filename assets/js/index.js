const TempAlert = 35;
const GasAlert = 400;


var ReqURL = "./API/";
var httpconfig = {
    headers: {
        'Content-Type': 'application/json',
    },
};
var CurrentRoom = 'LivingRoom';
var LastID;
var DoorTable;
$(function (e) {
    LoadHouse();
    LoadDoorHistory()

})
setInterval(function () {
    LoadHouse();
    LoadRoom(CurrentRoom);
    LoadLatestRow()
}, 1000);
$("#LoadLiving").click(function () {
    CurrentRoom = 'LivingRoom';
    LoadRoom(CurrentRoom);
    // console.log(CurrentRoom);
});
$("#LoadKitchen").click(function () {
    CurrentRoom = 'Kitchen';
    LoadRoom(CurrentRoom)
});

$('#LightChange').click(function () {
    var LightStatus;
    if (RoomData.Led == "On") {
        LightStatus = "Off";
    } else if (RoomData.Led == "Off") {
        LightStatus = "On";
    }
    var Req = {
        RoomName: CurrentRoom,
        Device: "Led",
        Status: LightStatus
    }
    $.post(ReqURL + "updateRoomDevice.php", Req);
    LoadRoom(CurrentRoom);
})
$('#FanChange').click(function () {
    var FanStatus;
    // console.log(RoomData);

    if (RoomData.Fan == "On") {
        FanStatus = "Off";
        // console.log(FanStatus);
    } else if (RoomData.Fan == "Off") {

        FanStatus = "On";
    };
    var Req = {
        RoomName: CurrentRoom,
        Device: "Fan",
        Status: FanStatus
    };
    // console.log(Req);
    $.post(ReqURL + "updateRoomDevice.php", Req);
    LoadRoom(CurrentRoom);

})

$('#LogOut').click(function (){
    var Req =
    {
        LogOut : true,
    }
    $.post("#",Req);
    location.reload();
})
function LoadRoom(RoomName) {
    var Req = {
        RoomName: RoomName
    }
    $.post(ReqURL + "getRoomStatus.php", Req, function (data) {
        switch (RoomName) {
            case "LivingRoom": RoomInVN = "Phòng khách";
                break;
            case "Kitchen": RoomInVN = "Phòng Bếp";
                break;
        }
        $("#RoomName").text(RoomInVN);
        ChangeFanDisplay(data.Fan);
        ChangeLightDisplay(data.Led);
        RoomData = data;
    })
}

function LoadHouse() {
    $.post(ReqURL + "getHouseStatus.php", function (data) {
        $("#HouseGas").text(data.Gas);
        $("#HouseTemp").text(data.Temp);
        $("#HouseHumid").text(data.Humid);
        if (data.Gas >= GasAlert)  $("#GasAlert").show();
        else $("#GasAlert").hide();
        if (data.Temp >= TempAlert) $("#TempAlert").show();
        else $("#TempAlert").hide();
        if (data.Motion == "Detected") $("#MotionAlert").show();
        else $("#MotionAlert").hide();
    })
};

function ChangeLightDisplay(Status) {
    if (Status == "Off") {
        $("#LightSlash").show();
        $("#LightChange").removeClass("btn-warning");
        $("#LightChange").addClass("btn-success");
        // $("#LightStatusTxt").text("Đèn tắt");
        $("#LightBtnTxt").text("Bật Đèn");
    } else if (Status == "On") {
        $("#LightSlash").hide();
        $("#LightChange").removeClass("btn-success");
        $("#LightChange").addClass("btn-warning");
        // $("#LightStatusTxt").text("Tắt Đèn");
        $("#LightBtnTxt").text("Tắt Đèn")
    }
}

function ChangeFanDisplay(Status) {
    if (Status == "Off") {
        $("#FanSlash").show();
        $("#FanChange").removeClass("btn-warning");
        $("#FanChange").addClass("btn-success");
        // $("#FanStatusTxt").text("Bật Quạt");
        $("#FanBtnTxt").text("Bật Quạt");
    } else if (Status == "On") {
        $("#FanSlash").hide();
        $("#FanChange").removeClass("btn-success");
        $("#FanChange").addClass("btn-warning");
        // $("#FanStatusTxt").text("Tắt fQuạt");
        $("#FanBtnTxt").text("Tắt quạt")
    };
}

function LoadDoorHistory() {
    var Req = {
        Row: 0
    }
    $.post(ReqURL + 'getDoorHistory.php', Req, function (data) {
        LastID = data[0].ID;
        data.forEach(element => {
            if (element['Status'] == "Open") {
                element['Status'] = "Cửa Mở";
            } else {
                element['Status'] = "Cửa Đóng";
            }
        });
        DoorTable = $('#example').DataTable(
            {
                "order": [[2, 'desc']],
                data: data,
                columns:
                    [
                        { data: 'Icons' },
                        { data: 'Status' },
                        { data: 'Time' }
                    ],
            }
        )
    })
    // $('#example').DataTable();
}

function LoadLatestRow() {
    var Req = {
        Row: 1
    };
	
    $.post(ReqURL + 'getDoorHistory.php', Req, function (data) {
		
        if(LastID != data[0].ID )
        {
			console.log(LastID);
			console.log(data[0]);
            LastID = data[0].ID;
            data.forEach(element => {
                if (element['Status'] == "Open") {
                    element['Status'] = "Cửa Mở";
					console.log(element);
					DoorTable.row.add(element).draw(true);
                } else if(element['Status'] == "Close")
				{
                    element['Status'] = "Cửa Đóng";
					console.log(element);
                }
            });
            DoorTable.row.add(data[0]).draw(true);
        }
    })
}