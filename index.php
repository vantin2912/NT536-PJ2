<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
	<?php
		require_once "./header.php";
		if(isset($_POST["LogOut"]) && $_POST["LogOut"] == TRUE)
		{
			session_unset();
			header("location: ./login.php");
			//session_destroy();
		}
?>
</head>

<body class="app sidebar-mini rtl">
	<div id="global-loader"></div>
	<div class="page" >
		<div class="page-main" >
			<!-- Sidebar menu-->
			<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
			<?php 
				require_once("./sidebar.php")
			?>
			
			<!-- Sidebar menu-->

			<!-- app-content-->
			<div class="app-content">
				<div class="side-app">
					<div class="main-content">
						<div class="p-2 d-block d-sm-none navbar-sm-search">
							<!-- Form -->
							<form class="navbar-search navbar-search-dark form-inline ml-lg-auto">
								<div class="form-group mb-0">
									<div class="input-group input-group-alternative">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-search"></i></span>
										</div>
										<input class="form-control" placeholder="Search" type="text" />
									</div>
								</div>
							</form>
						</div>
						<!-- Top navbar -->
						<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
							<div class="container-fluid">
								<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar"
									href="#"></a>

								<!-- Horizontal Navbar -->

								<!-- Brand -->
								<a class="navbar-brand pt-0 d-md-none" href="index-2.html">
									<img src="./assets/img/brand/logo-light.png" class="navbar-brand-img" alt="..." />
								</a>
								<h2><i class="fas fa-house-user"> </i> 
								<span id ="RoomName"></span>
								</h2>
							</div>
						</nav>
						<!-- Top navbar-->

						<!-- Page content -->

						<div class="container-fluid pt-8">
							<div class = "row mb-3" >
								<div class= "col-md-12">
									<div class="alert alert-danger alert-dismissible fade show mb-1" role="alert" id ="MotionAlert" style="display: none;">
										<span class="alert-inner--icon"><i class="fas fa-water danger"></i></span>
										<span class="alert-inner--text"><strong>Cảnh báo!</strong> Có chuyển động</span>

									</button>
									</div>
									<div class="alert alert-danger alert-dismissible fade show mb-1" role="alert" id ="GasAlert" style="display: none;">
										<span class="alert-inner--icon"><i class="fas fa-burn danger"></i></span>
										<span class="alert-inner--text"><strong>Cảnh báo!</strong> Khí Gas cao </span>

										</button>
									</div>
									<div class="alert alert-danger alert-dismissible fade show mb-1" role="alert" id= "TempAlert" style="display: none;">
										<span class="alert-inner--icon"><i class="fas fa-thermometer-three-quarters danger" ></i></span>
										<span class="alert-inner--text"><strong>Cảnh báo!</strong> Nhiệt độ cao </span>

										</button>
									</div>

								</div>
							</div>
							<div class="row justify-content-end">
								<div class="col-xl-4 col-lg-4">
									<div class="card pull-up shadow bg-gradient-primary">
										<div class="card-content">
											<div class="card-body">
												<img src="./assets/img/circle.svg" class="card-img-absolute"
													alt="circle-image" />
												<div class="media d-flex">
													<div class="media-body text-left">
														<h4 class="text-white">Độ ẩm</h4>
														<h2 class="text-white mb-0"><span id="HouseHumid"><span> %</h2>
													</div>
													<div class="align-self-center">
														<i
															class="fas fa-water success font-large-2 text-white float-right"></i>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-4 col-lg-4">
									<div class="card pull-up shadow bg-gradient-warning">
										<div class="card-content">
											<div class="card-body">
												<img src="./assets/img/circle.svg" class="card-img-absolute"
													alt="circle-image" />
												<div class="media d-flex">
													<div class="media-body text-left">
														<h4 class="text-white">Khí gas</h4>
														<h2 class="text-white mb-0" id="HouseGas"></h2>
													</div>
													<div class="align-self-center">
														<i
															class="fas fa-burn text-white font-large-2 float-right"></i>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-4 col-lg-4">
									<div class="card pull-up shadow bg-gradient-danger">
										<div class="card-content">
											<div class="card-body">
												<img src="./assets/img/circle.svg" class="card-img-absolute"
													alt="circle-image" />
												<div class="media d-flex">
													<div class="media-body text-left">
														<h4 class="text-white">Nhiệt độ</h4>
														<h2 class="text-white mb-0">
															<span id = "HouseTemp"></span> độ
														</h2>
													</div>
													<div class="align-self-center">
														<i
															class="fas fa-thermometer-three-quarters text-white font-large-2 float-right"></i>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>

							<div class="row">
								<div class="col-lg-6 col-xl-6">
									<div class="card shadow text-center">
										<div class="card-body">
											<div class="icon mx-auto">
												<span class="fa stack">
													<i class="fas fa-wind fa-7x fa-stack-1x"></i>
													<i class="fas fa-slash fa-stack-2x text-danger" id = "FanSlash"></i>
												</span>
											</div>
											<div class="text">
												<h1 class="mb-0" id ="FanStatusTxt">Quạt</h1>
												<!-- <label class="text-muted">Quạt</label> -->
											</div>
											<div class="options mt-3">
												<button id="FanChange" class="btn btn-md btn-warning">
													<span id="FanBtnTxt">Tắt Quạt</span>
												</button>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-xl-6">
									<div class="card shadow text-center">
										<div class="card-body">
											<div class="icon mx-auto">
												<span class="fa stack">
													<i class="fas fa-lightbulb fa-7x fa-stack-1x"></i>
													<i class="fas fa-slash fa-stack-2x text-danger" id = "LightSlash"></i>
												</span>
											</div>
											<div class="text">
												<h1 class="mb-0" id="LightStatusTxt">Đèn</h1>
												<!-- <label class="text-muted">Đèn</label> -->
											</div>
											<div class="options mt-3">
												<button id ="LightChange" class="btn btn-success btn-md">
													<span id="LightBtnTxt">Bật đèn</span></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class = "rơw">
							<div class ="col-12">
							<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Lịch sử cửa</h2>
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" style="text-align:center" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th class="wd-5p"></th>
															<th class="wd-15p">Trạng thái</th>
															<th class="wd-20p">Thời gian</th>

														</tr>
													</thead>
													<tbody id = "TableBody">
														
													</tbody>
												</table>
											</div>
										</div>
									</div>
							</div>
							</div>
							<!-- Footer -->
							<footer class="footer">
								<div class="row align-items-center justify-content-xl-between">
									<div class="col-xl-6">
										<div class="copyright text-center text-xl-left text-muted">
											<p class="text-sm font-weight-500">
												Copyright 2018 © All Rights Reserved.Dashboard
												Template
											</p>
										</div>
									</div>
									<div class="col-xl-6">
										<p class="float-right text-sm font-weight-500">
											<a href="www.templatespoint.net">Templates Point</a>
										</p>
									</div>
								</div>
							</footer>
							<!-- Footer -->
						</div>
					</div>
				</div>
			</div>
			<!-- app-content-->
		</div>
	</div>

	<?php 
				require_once "./footer.php";
		?>
	<script src="./assets/js/index.js"></script>
</body>

</html>