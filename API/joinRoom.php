<?php
    require_once "../Database/Query.php";
    require_once "./API-require.php";
    
    session_start();
    $rcv =  json_decode(file_get_contents("php://input"), true);
    // print_r($rcv) ;
    if(isset($rcv))
    {
        // print_r($_POST);
        
        $RoomID = $rcv["RoomID"];
        $ConversationID = $rcv["ConversationID"];
        $result = joinRoom($RoomID, $ConversationID);
        
        return $result;
    }
?>