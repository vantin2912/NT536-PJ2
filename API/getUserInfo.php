<?php
require_once "../Database/Query.php";
require_once "./API-require.php";

session_start();


$UserID = -1;

// print_r($_POST);
$rcv =  json_decode(file_get_contents("php://input"), true);

// print_r($rcv);
if(isset($rcv))
{
    if($rcv["UserID"] == -1)
    {
        $UserID = $_SESSION["UserID"];
    } else
    {
        $UserID = $rcv["UserID"];
    }
    $result = getUserInfo($UserID);
    // print_r($result);
    echo json_encode($result);
}

mysqli_close($conn);
?>