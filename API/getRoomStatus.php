<?php
    require_once "../Database/Query.php";
    require_once "./API-require.php";

    if(isset($_POST["RoomName"]))
    {
        switch ($_POST["RoomName"])
        {
            case "LivingRoom":
                $data = getRoomStatus(Living);
                echo json_encode($data);
            break;
            case "Kitchen":
                $data = getRoomStatus(Kitchen);
                echo json_encode($data);
            break;
        }
    }

    mysqli_close($conn);

?>