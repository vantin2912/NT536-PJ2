<?php
    require_once "../Database/Query.php";
    require_once "API-require.php";

    $Status = "Request Error";

    if(isset($_POST["sensor"]))
    {
        $SensorData = json_decode($_POST["sensor"],TRUE);
        // print_r($SensorData);
        if(!empty($SensorData))
        {
            
            $Status = updateHouseStatus($SensorData["HUMID"], $SensorData["TEMP"], $SensorData["GAS"]);
        }
        if(empty($Status))
        {
            SendRoomStatus();
        }
    }


    if(isset($_POST["devices"]))
    {
        // print_r($_POST);
        $roomdata = json_decode($_POST["devices"],TRUE);
        foreach($roomdata as $keys => $values)
        {
            if($values == 0)
            {
                $values = "Off";
            } else
            {
                $values = "On";
            }
            switch ($keys)
            {
                case "DK": 
                    updateRoomDevice(Living,"Led", $values);
                break;
                case "QK":
                    updateRoomDevice(Living,"Fan", $values);
                break;
                case "DB":
                    updateRoomDevice(Kitchen,"Led",$values);
                break;
                case "QB":
                    updateRoomDevice(Kitchen,"Fan",$values);
                break;
            }
        }
    }

    if(isset($_POST["door"]))
    {
        $DoorStatus = json_decode($_POST["door"],TRUE);
        updateDoorStatus($DoorStatus["DOOR"]);
    }

    if (isset($_POST["motion"]))
    {
        $MotionStatus = json_decode($_POST["motion"],TRUE);
        updateMotion($MotionStatus["Motion"]);
    }
    mysqli_close($conn);
?>