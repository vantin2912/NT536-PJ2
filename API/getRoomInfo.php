<?php
    require_once "../Database/Query.php";
    require_once "./API-require.php";
    
    session_start();
    $rcv =  json_decode(file_get_contents("php://input"), true);
    // print_r($rcv) ;
    if(isset($rcv))
    {
        
        
        $RoomID = $rcv["RoomID"];
        $result = getRoomInfo($RoomID);
        // print_r($result);
        echo json_encode($result);
        return $result;
    }
?>