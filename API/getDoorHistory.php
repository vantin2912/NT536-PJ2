<?php
    require_once "./API-require.php";
    if (isset ($_POST["Row"]))
    {
        $DoorHistory = getDoorHistory($_POST["Row"]);
        $Data = array();
        foreach ($DoorHistory as $Row)
        {
            if($Row["Status"] == "Close")
            {
                $Row["Icons"] = '<i class = "fas fa-door-closed"></i>' ;
            } else if($Row["Status"] == "Open")
            {
                $Row["Icons"] = '<i class ="fas fa-door-open"></i>';
            };
            array_push($Data,$Row);
        }
        echo json_encode($Data);    
    }
    mysqli_close($conn);
    
?>