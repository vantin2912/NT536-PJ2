<?php
    require_once "../Database/Query.php";
	header("Content-type: application/json; charset=utf-8");
    header('Access-Control-Allow-Origin: *');   
    define("Living",1);
    define("Kitchen",2);
    
    function SendRoomStatus()
    {
        $KitchenData = getRoomStatus(Kitchen);
        $LivingData = getRoomStatus(Living);
        $Data["DK"]= $LivingData["Led"];
        $Data["QK"]= $LivingData["Fan"];
        $Data["DB"]= $KitchenData["Led"];
        $Data["QB"]= $KitchenData["Fan"];
        foreach($Data as $Key => $value)
        {
            if($value=="On")
            {
                $Data[$Key] = 1 ;
            } else 
            {
                $Data[$Key] = 0;
            }
        }
        echo json_encode($Data);
    }
?>