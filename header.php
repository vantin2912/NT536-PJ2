<!DOCTYPE html>
<html lang="en" dir="ltr">

<?php
	@ob_start();
	session_start();
	require_once "./Database/Query.php";
	
	if(!isset($_SESSION["UserID"]))
	{
		// print_r($_SESSION);
		if(isset($_GET["room"]))
		{
			header('Location: ./login.php?room='.$_GET["room"]);
		}
		else
		{
			header('Location: ./login.php');
		}
		
		
	}
?>
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="Start your development with a Dashboard for Bootstrap 4." name="description">
	<meta content="Environment Station" name="">

	<!-- Title -->
	<title>NT536</title>

	<!-- Favicon -->
	<link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800" rel="stylesheet">

	<!-- Icons -->
	<link href="./assets/css/icons.css" rel="stylesheet">

	<!--Bootstrap.min css-->
	<link rel="stylesheet" href="./assets/plugins/bootstrap/css/bootstrap.min.css">

	<!-- Ansta CSS -->
	<link href="./assets/css/dashboard.css" rel="stylesheet" type="text/css">

	<!-- Custom scroll bar css-->
	<link href="./assets/plugins/customscroll/jquery.mCustomScrollbar.css" rel="stylesheet" />

	<!-- Data table css -->
	<link href="assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
	
	<!-- Sidemenu Css -->
	<link href="./assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

	
	<!-- Data tables -->
	<!-- <script src="assets/plugins/datatable/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatable/dataTables.bootstrap4.min.js"></script> -->

	<!-- Data table css -->
	<link href="assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />

</head>