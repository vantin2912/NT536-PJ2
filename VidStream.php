<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<!-- import the webpage's stylesheet -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css"/>
    <link rel="stylesheet" href="style.css"/>

	<?php
		require_once "./header.php";
		if(isset($_POST["LogOut"]) && $_POST["LogOut"] == TRUE)
		{
			session_unset();
			header("location: ./login.php");
			//session_destroy();
		}
?>

    <!-- import the webpage's javascript file -->

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios@0.20.0/dist/axios.min.js"></script>
    <script src="https://cdn.stringee.com/sdk/web/2.2.1/stringee-web-sdk.min.js"></script>

</head>

<body class="app sidebar-mini rtl">
	<div id="global-loader"></div>
	<div class="page" >
		<div class="page-main" >
			<!-- Sidebar menu-->
			<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
			<?php 
				require_once("./sidebar.php")
			?>
			<!-- Sidebar menu-->

			<!-- app-content-->
			<div class="app-content">
				<div class="side-app">
					<div class="main-content">
						<div class="p-2 d-block d-sm-none navbar-sm-search">
							<!-- Form -->
							<form class="navbar-search navbar-search-dark form-inline ml-lg-auto">
								<div class="form-group mb-0">
									<div class="input-group input-group-alternative">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-search"></i></span>
										</div>
										<input class="form-control" placeholder="Search" type="text" />
									</div>
								</div>
							</form>
						</div>
						<!-- Top navbar -->
						<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
							<div class="container-fluid">
								<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar"
									href="#"></a>
								<p class="navbar-nav align-items-center" id="RoomID">
									
								</p>
								
								<!-- Horizontal Navbar -->

								<!-- Brand -->
								<a class="navbar-brand pt-0 d-md-none" href="./VidStream.php">
									<img src="./assets/img/brand/logo.png" class="navbar-brand-img" alt="..." />
								</a>
								<h2><i class="fas fa-video"> </i> 
								<span> Video Conference Nhóm 6</span>
								</h2>
							</div>
						</nav>
						<!-- Top navbar-->

						<!-- Page content -->

						<div class="container-fluid pt-8" >
							<div class="page-header mt-0 shadow p-3">
								<ol class="breadcrumb mb-sm-0">
									<li class="breadcrumb-item"><a href="#">NT536</a></li>
									<li class="breadcrumb-item active" aria-current="page">Video Conference</li>
								</ol>
								<div class ="btn-group ">
									
								</div>
								<div class="btn-group mb-0">
									<button type="button" class ="btn btn-success btn-sm pr-1"  data-toggle="modal" data-target="#ChatModal" id="MessageBTN">
										<i class="fas fa-comment-dots"></i>
										Tin nhắn 
										<span class="badge bg-red badge-red" id="AlertNewMessage" hidden>!</span> 
									</button>
									<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
									<div class="dropdown-menu">
										<a class="dropdown-item" id="ShareScreen"><i class="fas fa-share-square mr-2"></i>ShareScreen</a>
										<a class="dropdown-item" id="MuteAudio"><i class="fas fa-microphone mr-2" id="MuteIcon"></i><span id="MuteMsg">Tắt tiếng</span></a>
										<a class="dropdown-item" id="DisableCamera"><i class="fas fa-camera mr-2" id="CameraIcon"></i><span id="CameraMsg">Tắt Camera</span></a>
										
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-cog mr-2"></i> Settings</a>
									</div>
								</div>
							</div>
							<div v-cloak id="app">
								<div class="row" v-if="!room">
									<div class="col-xl-6 col-lg-6">
										<div class="card shadow text-center">
											<div class="card-body">
												<div class="icon mx-auto">
													<span class="fa stack">
														<i class="fas fa-video fa-7x fa-stack-1x"></i>
													</span>
												</div>
												<div class="options mt-3">
													<button class="btn btn-md btn-primary"  @click="createRoom">
														<span>Tạo Meeting</span>
													</button>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xl-6 col-lg-6">
										<div class="card shadow text-center">
											<div class="card-body">
												<div class="icon mx-auto">
													<span class="fa stack">
														<i class="fas fa-plus-circle fa-7x fa-stack-1x"></i>
													</span>
												</div>
												<div class="options mt-3">
													<button class="btn btn-success btn-md" @click="joinWithId">
														<span>Join Meeting</span></button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="RoomCode"></div>
							</div>
							<div class ="row">
								<div class ="col-xl-12">
									<div class = "unsortable">
										<div class ="row" id="VideosBlock">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div>
								
							

							<!-- Footer -->
							
							<footer class="footer">
								<div class="row align-items-center justify-content-xl-between">
									<div class="col-xl-6">
										<div class="copyright text-center text-xl-left text-muted">
											<p class="text-sm font-weight-500">
												Copyright 2018 © All Rights Reserved.Dashboard
												Template
											</p>
										</div>
									</div>
									<div class="col-xl-6">
										<p class="float-right text-sm font-weight-500">
											<a href="www.templatespoint.net">Templates Point</a>
										</p>
									</div>
								</div>
							</footer>
							
							<!-- Footer -->

							<!-- Modal-->
							<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h2 class="modal-title" id="exampleModalLabel">Cài đặt</h2>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label>Chọn Camera</label>
														<select class="form-control select2 w-100" id="ListCamera">
															<option selected="selected" value="">Chọn Camera</option>
														</select>
														
													</div>

												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Chọn Loa</label>
														<select class="form-control select2 w-100" id="ListSpeaker">
															<option selected="selected" value="">Chọn Loa</option>
														</select>
														
													</div>

												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Chọn Micro</label>
														<select class="form-control select2 w-100" id="ListMicro">
															<option selected="selected" value="">Chọn Micro</option>
														</select>
														
													</div>

												</div>
												
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
										</div>
									</div>
								</div>
							</div>

							<div class="modal fade" id="ChatModal" tabindex="-1" role="dialog" aria-labelledby="ChatModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h2 class="modal-title" id="ChatModelLabel">Tin nhắn</h2>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
										<div class="content mscroll" id="MessageBody">
										</div>
										</div>
										<div class="modal-footer">
											<div class="input-group">
													<input type="text" class="form-control" id="InputMessage" placeholder="Type to send...">
													<span class="input-group-append">
														<button class="btn btn-primary" type="button" id="SendMessage">Gửi</button>
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Modal-->


						</div>
					</div>
				</div>
			</div>
			<!-- app-content-->
		</div>
	</div>
	<!-- Video Stream JS -->
	
		
	<?php 
				require_once "./footer.php";
		?>
	<script src="./VideoStream/api.js"></script>
	<script src="./VideoStream/script.js"></script>
	<!-- <script>
		$(function () {
			// ______________Dragable cards
			// sortable
			$(".sortable").sortable({
				connectWith: '.sortable',
				items: '.card-draggable',
				revert: true,
				placeholder: 'card-draggable-placeholder',
				forcePlaceholderSize: true,
				opacity: 0.77,
				cursor: 'move'
			});
		});
	</script> -->
</body>

</html>