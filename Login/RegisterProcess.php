<?php 
    require_once "./Database/Query.php";
    
    if(isset($_SESSION["UserID"]))
    {
        header('Location: ./VidStream.php');
    }
    if(isset($_POST["BtnRegister"]) && $_POST["BtnRegister"] == "Register")
    {
        
        $Email = $_POST["Email"];
        $Pass = $_POST["Password"];
        $FullName = $_POST["FullName"];
        $UserName = $_POST["Username"];


        $result = addUser($UserName, $Email, $FullName, $Pass);
        if($result)
        {
            $userID = CheckUser($Email, $Pass);
            session_start();
            $_SESSION["UserID"] = $userID;
            header('Location: ./VidStream.php');
        }
        else
        {
            echo "Trùng username hoặc Email";
        }

    }
?>