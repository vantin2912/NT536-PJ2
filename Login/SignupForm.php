<?php 
require_once "./Login/RegisterProcess.php";
?>
		
        <form method ="post" class="login100-form validate-form">
			<span class="login100-form-title">
				Đăng ký
			</span>

			<div class="wrap-input100 validate-input" data-validate = "Nhập Email">
				<input class="input100" type="email" name="Email" placeholder="Email" required>
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</span>
			</div>

			<div class="wrap-input100 validate-input" data-validate = "Nhập Username">
				<input class="input100" type="text" name="Username" placeholder="Username" required>
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</span>
			</div>

			<div class="wrap-input100 validate-input" data-validate = "FullName is required">
				<input class="input100" type="text" name="FullName" placeholder="Họ Tên" required>
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</span>
			</div>

			<div class="wrap-input100 validate-input" data-validate = "Password is required">
				<input class="input100" type="password" name="Password" placeholder="Password" required>
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-lock" aria-hidden="true"></i>
				</span>
			</div>

			<div class="container-login100-form-btn">
				
				<button type="submit" name = "BtnRegister" value ="Register" class="btn btn-pill btn-primary mt-1 mb-1">
				Đăng ký
				</button>
			</div>
			<div class="text-center pt-1">
				<a class="txt2" href="./login.php">
					Đăng nhập
					<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
				</a>
			</div>
		</form>