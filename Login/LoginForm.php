<?php 
require_once "./Login/process.php";
?>
		
        <form method ="post" class="login100-form validate-form">
			<span class="login100-form-title">
				Đăng nhập
			</span>

			<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
				<input class="input100" type="email" name="Email" placeholder="Email">
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</span>
			</div>

			<div class="wrap-input100 validate-input" data-validate = "Password is required">
				<input class="input100" type="password" name="Password" placeholder="Password">
				<span class="focus-input100"></span>
				<span class="symbol-input100">
					<i class="fa fa-lock" aria-hidden="true"></i>
				</span>
			</div>

			<div class="container-login100-form-btn">
				<button type="submit" name = "BtnLogin" value ="Login" class="btn btn-pill btn-primary mt-1 mb -1">
				Đăng nhập
				</button>
			</div>
			<div class="text-center pt-1">
				<a class="txt2" href="./signup.php">
					Đăng ký
					<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
				</a>
			</div>
		</form>