// 1. Tổng quan

//     - Khi trang web đc load thì mouted() sẽ hoạt động tìm RoomID trên URL để tiến hành vào room:
//        Luồng chạy: mounted() -> join() -> authen() và publish()

//     - Nếu không tìm thấy RomID trên URL thì User có thể thực hiện các tính năng:
//          + Tạo room: Gọi API Stringbee để tiến hành tạo room và vào room
//             Luồng chaỵ: createRoom() -> authen() và publish()
//          + Join bằng ID:  lấy RoomID để tiến hành vào room
//             Luồng chaỵ: joinWithId() -> join() -> authen() và publish()

// 	  - Cách để vào room
//	.       + Xác thực user với authen() để kết nối đến system Stringbee
//          + Vào room khi đã xác thực với publish() và lắng nghe event thêm, xoá track video

// 2. Phân tích code
const videoContainer = document.getElementById("videos"); // Chứa tất cả các track trong room
const VideoBlock = document.getElementById("VideoBlock");

const vm = new Vue({
  el: "#app",
  data: {
    userToken: "", // dùng cho callClient
    roomId: "", // dùng để join vào room bằng ID và tạo roomToken
    roomToken: "", // dùng để kết nối vào room
    room: undefined, //dùng để lắng nghe sự kiện xảy ra: xoá track khi user thoát, thêm track khi user vào
    localTracks: undefined,
    screenTracks: undefined,
    callClient: undefined, //dùng để kết nối đến system Stringbee bằng userToken và sau đó dùng để join room
    UserInRoom: [],
    Chat: undefined,
    ConversationID: "",
    UserID: -1
  },
  computed: {

    // tạo URL để mời user vào room
    roomUrl: function () {
      return `https://${location.hostname}?room=${this.roomId}`;
    }
  },


  // Hàm mounted() này sẽ chạy mỗi lần load trang để tìm Romm ID join vào room được mời bằng URL
  async mounted() {
    // Khởi tạo rest_access_token để tạo rom hoặc join rom
    api.setRestToken();

    // VD: URL =  https://stg-vid-call.glitch.me?room=room-vn-1-R4DTIJFRPT-1633694007964
    // lấy param "room" trên URL VD như URL trên thì romID = room-vn-1-R4DTIJFRPT-1633694007964
    const urlParams = new URLSearchParams(location.search);
    const roomId = urlParams.get("room");

    // Nếu tồn tại romID thì sẽ tiến hành join() room bằng romID
    if (roomId) {
      this.roomId = roomId; //Có RoomID
      this.ShowRoomURL(this.roomId);
      await this.join(); // Gọi join()
    }
  },


  methods: {

    // hàm authen() để xác thực user kết nối đến system của Stringbee
    authen: function () {
      return new Promise(async resolve => {

        // Lấy UserToken từ API của Stringbee thông qua userID random
        const localInfo = await api.getUserInfo(-1);
        // const userId = `${(Math.random() * 100000).toFixed(6)}`;
        this.UserID = localInfo.UserID;
        const userToken = await api.getUserToken(this.UserID);
        this.userToken = userToken;
        // console.log({localInfo.UserID, userToken});

        // Nếu chưa tạo kết nối đến System Stringbee thì sẽ thực hiên việc khởi tạo
        if (!this.callClient) {
          // Khởi tạo kết nối đến System Stringbee
          const client = new StringeeClient();
          client.on("authen", function (res) {
            resolve(res);
          });
          this.callClient = client;
        }
        if (!this.Chat) {
          this.Chat = new StringeeChat(this.callClient);
        }
        // Kết nối đến System Stringbee bằng userToken
        this.callClient.connect(userToken);
      });
    },
    publish: async function (screenSharing = false) {

      // tạo track video của local bằng input là callClient và setting

      var Tracks = await StringeeVideo.createLocalVideoTrack(
        this.callClient,
        {
          audio: true,
          video: true,
          record: false,
          screen: screenSharing, // setting share màn hình
          videoDimensions: { width: 600, height: 360 }
        }
      );

      if (screenSharing == true) {
        screenTracks = Tracks;
      }
      else {
        localTracks = Tracks;
      }

      // Gắn track local  vào Elenment HTML

      const videoElement = Tracks.attach();


      this.addVideo(videoElement);
      this.JoinUser(Tracks.localId, -1);
      await this.RenderTrack(Tracks);

      // Join vào room bằng input callClient và roomToken
      const roomData = await StringeeVideo.joinRoom(
        this.callClient,
        this.roomToken
      );
      const room = roomData.room;



      // Lắng nghe các event
      if (!this.room) {
        this.room = room;
        room.clearAllOnMethos();
        // Event thêm track video vào elenment HTML khi có user mới vào room
        room.on("addtrack", e => {
          const track = e.info.track;
          if (track.serverId === localTracks.serverId) { //kiểm tra nếu track id trùng với track id local thì không thêm

            return;
          }
          this.subscribe(track); // Gọi subscribe() đăng ký track
          console.log("Subscribe track", track.publishId);
        });
        room.on("leaveroom",e =>{
          console.log("leaveroom", e);
        })
        // Event xoá track video khỏi elenment HTML khi user mới thoát room
        room.on("removetrack", e => {
          const track = e.track;
          console.log("removetrack", e);
          if (!track) {
            return;
          }
          var TrackID = track.localId;
          const mediaElements = track.detach();
          
          console.log("RemoveTrack: ", mediaElements);
          mediaElements.forEach(element => element.remove());
          var el = $("#" + TrackID + "-col").remove();
          // console.log(el);
        });

        this.Chat.on('onObjectChange', info => {
          console.log("Rcv Msg Info", info);
          if (info.objectType == 1) {
            if (info.changeType == 0) {
              const Obj = info.objectChanges;
              Obj.forEach(async Msg => {
                console.log(this.ConversationID);
                console.log(Msg);
                if (this.Chat.client.userId == Msg.sender) {
                  return;
                }
                if(this.ConversationID !=  Msg.conversationId)
                {
                  return;
                }
                this.RenderChat(Msg)
              }
              );
            }

          }
        });
        // Hiển thị tất cả các track video của user đã vào  room
        roomData.listTracksInfo.forEach(info => this.subscribe(info));

      }

      await room.publish(Tracks); // public track video user
      console.log("room publish successful");
    },

    // tạo room
    createRoom: async function () {
      //  Tạo RoomID từ API của Stringbee  và tạo RoomToken
      const room = await api.createRoom();
      const { roomId } = room;
      const roomToken = await api.getRoomToken(roomId);
      this.roomId = roomId;
      this.ShowRoomURL(this.roomId);
      this.roomToken = roomToken;


      await this.authen(); // xác thực

      const options = {
        name: "Conversation Room: " + this.roomId,
        isDistinct: false,
        isGroup: true
      };
      
      const UIDconv= Array.from(Array(100).keys());
      UIDconv.splice(this.UserID, 1);
      // console.log(UIDconv.map(String));

      const conver = await this.Chat.createConversation(UIDconv.map(String), options);
      this.ConversationID = conver.convId;
      api.joinRoom(this.roomId, this.ConversationID);

      await this.publish(); // join room

    },

    // Hàm join() vào room vs đk là phải có romID
    join: async function () {
      const roomToken = await api.getRoomToken(this.roomId); // Gọi API của Stringbee lấy RoomToken
      this.roomToken = roomToken;

      // Tiến hành vào room
      await this.authen(); // xác thực
      const roomInfo = await api.getRoomInfo(this.roomId);
      this.ConversationID = roomInfo.ConversationID;
      await this.publish(); // join room

    },

    // Hàm joinWithId() lấy Room ID để join()
    joinWithId: async function () {
      const roomId = prompt("Dán Room ID vào đây nhé!");
      if (roomId) {
        this.roomId = roomId; // có RoomID
        this.ShowRoomURL(this.roomId);
        await this.join(); // Gọi method join() phía trên
      }
    },

    // Đăng ký các track video của user khác vào elenment HTML
    subscribe: async function (trackInfo) {
      const track = await this.room.subscribe(trackInfo.serverId);
      console.log(trackInfo);
      this.JoinUser(track.localId, trackInfo.userPublish);
      // stringeeChat.addParticipants(this.ConverId, [trackInfo.userPublish], function (status, code, message, added) {
      //   console.log('status:' + status + ' code:' + code + ' message:' + message + 'added: ' + JSON.stringify(added));
      // });

      track.on("ready", () => {
        const videoElement = track.attach();
        const UID = trackInfo.userPublish;
        this.RenderTrack(track);
        this.addVideo(videoElement, UID); //thêm track
      });
    },

    // Thêm track video vào elenment HTML
    addVideo: function (video) {
      video.setAttribute("controls", "true");
      video.setAttribute("playsinline", "true");
      // videoContainer.appendChild(video);
    },

    RenderTrack: async function (TrackObj) {
      const TrackID = TrackObj.localId;
      const User = this.UserInRoom.find(function (item) {
        return item.TrackID == TrackID
      });
      console.log({ TrackID, User });
      const Info = await api.getUserInfo(User.UserID);
      const VideoElement = TrackObj.attach();
      VideoElement.setAttribute("controls", "true");
      VideoElement.setAttribute("playsinline", "true");

      var Card = [{ TrackID: TrackID, Title: Info.FullName }].map(VideoCard).join('');
      $("#VideosBlock").prepend(Card);
      $("#" + TrackID).append(VideoElement);
    },

    DeleteRoom: function () {
      api.deleteRoom(this.roomId);
      this.roomId = "";
      this.roomToken = "",
        this.room = undefined;
      this.localTracks = undefined;
      this.screenTracks = undefined;
    },

    JoinUser: function (TrackID, UserID) {
      this.UserInRoom.push({
        TrackID,
        UserID
      })

    },

    RenderChat: async function (MsgObj, isSender = false) {
      $("AlertNewMessage").show();
      const SendTime = new Date(MsgObj.createdAt);
      const Info = await api.getUserInfo(MsgObj.sender);
      const Card = [{ MessageID: MsgObj.id, Message: MsgObj.content.content, FullName: Info.FullName, Time: SendTime.toLocaleTimeString() }]
        .map(ChatCard).join('');
      $("#MessageBody").append(Card);
      
      if (isSender) {
        $("#" + MsgObj.id).addClass("text-right");
      }
    },

    SendTxtMessage: async function (Message) {
      if (this.ConversationID === "")
        return;
      var txtMsg = {
        type: 1,
        convId: this.ConversationID,
        message: {
          content: Message,
          metadata: {
            key: 'value'
          }
        }
      }
      await this.Chat.sendMessage(txtMsg, async (status, code, message, msg) => {
        if (code == 0) {
          $("#InputMessage").val("");
          this.RenderChat(msg, true);
        }
      });

    },
    MuteAudio: function () {

      if (localTracks.muted) {
        //unmute
        console.log('Bật tiếng');
        localTracks.mute(false);
        $('#MuteMsg').html('Tắt tiếng');
      } else {
        //mute
        console.log('Tắt tiếng');
        localTracks.mute(true);
        $('#MuteMsg').html('Bật tiếng');
      }

    },

    DisableCamera: function () {
      if (localTracks.screen) {
        return;
      }

      console.log('hien tai track.localVideoEnabled=' + localTracks.localVideoEnabled);

      if (localTracks.localVideoEnabled) {
        //disable
        localTracks.enableLocalVideo(false);
        $('#CameraMsg').text('Bật Camera');
      } else {
        //enable
        localTracks.enableLocalVideo(true);
        $('#CameraMsg').text('Tắt Camera');
      }
    },

    ChangeDevice: function (device, deviceID) {
      localTracks.ChangeDevice(device, deviceID);
    },

    routeAudioToSpeaker: function (SpeakerID) {
      localTracks.routeAudioToSpeaker(SpeakerID)
    },

    HaveLocalTrack() {
      return localTracks ? true : false;
    },

    ShowRoomURL(RoomID) {
      var RoomURL = location.origin + location.pathname + "?room=" + RoomID;
      // var RoomURL = location.origin + location.pathname + "?room=" + "abcxxyz";
      $("#RoomID").text(RoomURL + "abcxyz");
    }
  }
});



const VideoCard = ({ TrackID, Title }) => `
  <div class="col-xl-4 col-lg-6 col-sm-12" id="${TrackID}-col">
  <div class="card card-draggable shadow">
    <div class="embed-responsive embed-responsive-16by9" id = ${TrackID}>
      
    </div> 
    <div class="media card-body media-xs overflow-visible">
      <a class="media-left" href="javascript:;"><img alt="" class="avatar avatar-md img-circle" src="./assets/img/avatar-4.png"></a>
      <div class="media-body valign-middle halign-middle">
        <b class="text-inverse pb-4">${Title}</b>
      </div>
    </div>
       
    
  </div>
  </div>
`;

const ChatCard = ({ MessageID, Message, FullName, Time }) => `
  <div class="pb-4" id="${MessageID}">
  <div> <img src="./assets/img/avatar-4.png" class="rounded-circle mr-1" alt="Sharon Lessman" width="40" height="40">
    <div class="text-muted small text-nowrap mt-2">${Time}</div>
  </div>
  <div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3">
    <div class="font-weight-bold mb-1">${FullName}</div> 
    ${Message}
  </div>

  </div>
`;

function getAllFuncs(toCheck) {
  const props = [];
  let obj = toCheck;
  do {
    props.push(...Object.getOwnPropertyNames(obj));
  } while (obj = Object.getPrototypeOf(obj));

  return props.sort().filter((e, i, arr) => {
    if (e != arr[i + 1] && typeof toCheck[e] == 'function') return true;
  });
}

$("#MessageBTN").click(function()
{
  $("AlertNewMessage").hide();
})
$("#ShareScreen").click(async function () {
  // var lst  = await api.listRoom();
  // for(let i = 0; i <lst.length; i++)
  // {
  //   api.deleteRoom(lst[i].roomId);
  // }
  vm.publish(true);
})

$("#SendMessage").click(function () {
  const Txt = $("#InputMessage").val();
  vm.SendTxtMessage(Txt);
})

$("#MuteAudio").click(function () {
  vm.MuteAudio();
})

$("#DisableCamera").click(function () {
  vm.DisableCamera();
})
$("#DeleteRoom").click(function () {
  vm.DeleteRoom();
})

$("#LoadLiving, #LoadKitchen").click(function () {
  console.log("Hello ");
  window.location.href = "./";
})


$('#LogOut').click(function () {
  var Req =
  {
    LogOut: true,
  }
  $.post("#", Req);
  location.reload();
})

var selectedCameraId = null;
var selectedMicrophoneId = null;
var selectedSpeakerId = null;

$(document).ready(function () {

  // console.log(Object.getOwnPropertyNames(StringeeUtil));
  // console.log(StringeeUtil);
  // console.log(typeof StringeeUtil.name);

  $("#ListCamera").change(function () {
    selectedCameraId = $("#listCameras").val();
    localStorage.setItem("selectedCameraId", selectedCameraId);
    console.log('selectedCameraId===' + selectedCameraId);

    if (!vm.HaveLocalTrack()) {
      return;
    }
    vm.changeDevice('video', selectedCameraId);
  });

  $("#ListMicro").change(function () {
    selectedMicrophoneId = $("#listMicrophones").val();
    localStorage.setItem("selectedMicrophoneId", selectedMicrophoneId);

    if (!vm.HaveLocalTrack()) {
      return;
    }
    vm.changeDevice('audio', selectedMicrophoneId);
  });

  $("#ListSpeaker").change(function () {
    selectedSpeakerId = $("#listSpeakers").val();
    localStorage.setItem("selectedSpeakerId", selectedSpeakerId);

    subscribedTracks.forEach(track => {
      vm.routeAudioToSpeaker(selectedSpeakerId);
    });
  });

  //ham nay goi khi vao page Check Devices
  // StringeeVideo.getDevicesInfo().then(function (data) {
  //     console.log('StringeeVideo.getDevicesInfo()..., thu lay thong tin ve thiet bi');
  //     renderDevicesInfo(data);
  // });
});




function renderDevicesInfo(data) {//neu User chua cap quyen thi ham nay tra ve da rong~
  selectedCameraId = localStorage.getItem("selectedCameraId");
  selectedMicrophoneId = localStorage.getItem("selectedMicrophoneId");
  selectedSpeakerId = localStorage.getItem("selectedSpeakerId");

  console.log('local storage: selectedMicrophoneId=====' + selectedMicrophoneId);

  $('#ListCamera').html('');
  $('#ListMicro').html('');
  $('#ListSpeaker').html('');

  var selectedCameraOk;
  var selectedMicOk;
  var selectedSpeakerOk;

  data.cameras.forEach((camera, index) => {
    var option = $('<option>').val(camera.deviceId).text(camera.label);
    if (selectedCameraId && selectedCameraId === camera.deviceId) {
      option.attr('selected', 'selected');
      selectedCameraOk = true;
    }

    $('#ListCamera').append(option);
  });
  data.microphones.forEach((mic, index) => {
    var option = $('<option>').val(mic.deviceId).text(mic.label);
    if (selectedMicrophoneId && selectedMicrophoneId === mic.deviceId) {
      option.attr('selected', 'selected');
      selectedMicOk = true;
    }

    $('#ListMicro').append(option);//deviceId
  });

  data.speakers.forEach((speaker, index) => {
    var option = $('<option>').val(speaker.deviceId).text(speaker.label);
    if (selectedSpeakerId && selectedSpeakerId === speaker.deviceId) {
      option.attr('selected', 'selected');
      selectedSpeakerOk = true;
    }

    $('#ListSpeaker').append(option);//deviceId
  });

  if (selectedCameraId && !selectedCameraOk) {
    localStorage.removeItem("selectedCameraId");
    selectedCameraId = null;
  }
  if (selectedMicrophoneId && !selectedMicOk) {
    localStorage.removeItem("selectedMicrophoneId");
    selectedMicrophoneId = null;
  }
  if (selectedSpeakerId && !selectedSpeakerOk) {
    localStorage.removeItem("selectedSpeakerId");
    selectedSpeakerId = null;
  }
}