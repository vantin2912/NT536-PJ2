<?php

    require_once "../Database/Query.php";
    $DoorData = getDoorHistory();
    mysqli_close($conn);
    $Data = array();
    foreach ($DoorData as $Row)
    {
        if($Row["Status"] == "Close")
        {
            $Row["Icons"] = '<i class = "fas fa-door-closed"></i>' ;
        } else if($Row["Status"] == "Open")
        {
            $Row["Icons"] = '<i class ="fas fa-door-open"></i>';
        };
        array_push($Data,$Row);
    }
    echo json_encode($Data);
    ?>
